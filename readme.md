# VagrantOak - Symfony2 Laravel's Homestead Adaptation

This project is a fork of the [Laravel](http://laravel.com/) Homestead local development environment adapted to the [Symfony2](http://symfony.com/) framework.

## Requirements

The VagrantOak requires [Vagrant](http://vagrantup.com), [Virtualbox](http://virtualbox.org) to work.

## Installation

### Install composer

To install VagrantOak you will need [Composer](http://getcomposer.org).

In Linux or Mac OSx you can install via terminal:

```
curl -sS https://getcomposer.org/installer | php
```

Then you can move to a global path so it's accessible system wide.

```
sudo mv composer.phar /usr/bin/composer
```

### Install VagrantOak with composer

```
composer global require ventureoak/vagrantoak
```
If it's the first time you use composer globaly, you should ensure the ~/.composer/vendor/bin is in your Path by adding the next code to the end of **.profile** file
```
if [ -d "$HOME/.composer/vendor/bin" ]; then
   PATH="$HOME/.composer/vendor/bin:$PATH"
fi
```

## Usage

### Initialization

To initialize a VagrantOak machine use the **init** option.

```
vagrantoak init
```

### Configuration

To configure your environment use the **edit** option.

```
vagrantoak edit
```

#### Parameters

##### ip

The ip address of the VagrantOak machine to your system.

##### memory

The ammount of RAM memory of the virtual machine.

#### cpus

The number of virtual CPU's of the virtual machine.

#### authorize

The SSL key to allow the communication between virtual and host machines.

#### folders

The folder mapping from your host machine **(map)** to be available on the virtual machine **(to)**.

#### sites

The sites you want to configure on the virtual machine. The configuration will be executed where you run the **vagrantoak provision** or **vagrantoak up --provision** commands.

#### databases

The databases you want to automatically create on the virtual machine.

#### variables

The environment variables you want available on the virtual machine.

### Set your SSL key

To allow the access over SSH you must set your SSL key.

```
ssh-keygen -t rsa -C "you@domain.com"
```

#### Example

This is an example of the implementation of a Symfony2 application in the **symfony.app** url.

1. Associate the domain to the **/etc/hosts** file to the **192.168.10.10** IP address

```
(...)
192.168.10.10   symfony.app
(...)
```

2. Edit the vagrantoak to make the new application available for your host system
```
---
ip: "192.168.10.10"
memory: 2048
cpus: 1

authorize: ~/.ssh/id_rsa.pub

keys:
    - ~/.ssh/id_rsa

folders:
    - map: ~/Code
      to: /home/vagrant/Code

sites:
    - map: symfony.app
      to: /home/vagrant/Code/symfony.app/web
      framework: symfony2 #phalconphp

databases:
    - vagrantoak

variables:
    - key: APP_ENV
      value: local
```

### Commands

#### Initialize the structure

```
vagrantoak init
```

#### Edit the configuration

```
vagrantoak edit
```

#### Start the machine

```
vagrantoak up
```

#### Stop the machine

```
vagrantoak halt
```

#### Provision the configuration

```
vagrantoak provision
```

#### Access te machine over SSH

```
vagrantoak ssh
```

## Database Access

### Mysql

#### Root access

*username:* root
*password:* secret


#### User acess

*username:* vagrantoak

*password:* secret


## Symfony2

### Development access

To use the development mode in Symfony2 you must add the 192.168.10.1 IP to the allowed list.

```
if (isset($_SERVER['HTTP_CLIENT_IP'])
    || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
    || !(in_array(@$_SERVER['REMOTE_ADDR'], array('127.0.0.1', 'fe80::1', '::1', '192.168.10.1')) || php$
) {
    header('HTTP/1.0 403 Forbidden');
    exit('You are not allowed to access this file. Check '.basename(__FILE__).' for more information.');
}
```

### Performance improvment

Using Symfony2 inside a Vagrant box is considered to be very slow. If your application is slow, just add the following code to the AppKernel.php file:

```
<?php

class AppKernel extends Kernel
{
    // ...

    public function getCacheDir()
    {
        if (in_array($this->environment, array('dev', 'test'))) {
            return '/dev/shm/appname/cache/' .  $this->environment;
        }

        return parent::getCacheDir();
    }

    public function getLogDir()
    {
        if (in_array($this->environment, array('dev', 'test'))) {
            return '/dev/shm/appname/logs';
        }

        return parent::getLogDir();
    }
}
```
NOTE: The directories *dev/shm/appname/cache/* and */dev/shm/appname/logs* must be created with 777 permissions.

## Phalcon PHP

### Phalcon tool

It's available on the terminal the phalcon console tool using the phalcon.php command.

#### Create a project

```
phalcon.php create-project <name>
```

### Official documentation

The official Laravel local development environment.

Official documentation [is located here](http://laravel.com/docs/homestead?version=4.2).
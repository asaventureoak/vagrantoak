<?php namespace VentureOak\VagrantOak;

use Symfony\Component\Process\Process;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InitCommand extends Command {

	/**
	 * Configure the command options.
	 *
	 * @return void
	 */
	protected function configure()
	{
		$this->setName('init')
                  ->setDescription('Create a stub VagrantOak.yaml file');
	}

	/**
	 * Execute the command.
	 *
	 * @param  \Symfony\Component\Console\Input\InputInterface  $input
	 * @param  \Symfony\Component\Console\Output\OutputInterface  $output
	 * @return void
	 */
	public function execute(InputInterface $input, OutputInterface $output)
	{
		if (is_dir(vagrantoak_path()))
		{
			throw new \InvalidArgumentException("VagrantOak has already been initialized.");
		}

		mkdir(vagrantoak_path());

		copy(__DIR__.'/stubs/VagrantOak.yaml', vagrantoak_path().'/VagrantOak.yaml');
		copy(__DIR__.'/stubs/after.sh', vagrantoak_path().'/after.sh');
		copy(__DIR__.'/stubs/aliases', vagrantoak_path().'/aliases');

		$output->writeln('<comment>Creating VagrantOak.yaml file...</comment> <info>✔</info>');
		$output->writeln('<comment>VagrantOak.yaml file created at:</comment> '.vagrantoak_path().'/VagrantOak.yaml');
	}

}

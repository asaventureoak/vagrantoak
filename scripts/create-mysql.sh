#!/usr/bin/env bash

DB=$1;
mysql -uvagrantoak -psecret -e "DROP DATABASE IF EXISTS $DB";
mysql -uvagrantoak -psecret -e "CREATE DATABASE $DB";

#!/usr/bin/env bash

mkdir -p ~/.vagrantoak

cp src/stubs/VagrantOak.yaml ~/.vagrantoak/VagrantOak.yaml
cp src/stubs/after.sh ~/.vagrantoak/after.sh
cp src/stubs/aliases ~/.vagrantoak/aliases

echo "VagrantOak initialized!"
